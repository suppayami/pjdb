<?php
require_once("./include/g9projectdb_config.php");
if(!isset($_GET['userid']))
  {
    $g9projectdb->RedirectToURL("no_permission.php");
  }
include 'header.php';
?>

<div id="project-detail">
	<h2 class="sub-header">Profile Detail</h2>

<?php
if(isset($_GET['userid']))
{
    $g9projectdb->ShowUser();
}
?>
</div>

<?php include 'footer.php'; ?>