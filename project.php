<?php
require_once("./include/g9projectdb_config.php");

if(!$g9projectdb->CheckLogin())
{
    $g9projectdb->RedirectToURL("notlogin.php");
    exit;
}
if(!isset($_GET['workid']))
{
  $g9projectdb->RedirectToURL("no_permission.php");
}
include 'header.php';
?>

<div id="project-detail">
	<h2 class="sub-header">Project Detail</h2>

<?php
$g9projectdb->ShowWork();
?>


<?php
if(isset($_GET['workid']))
{
  if(!$g9projectdb->CheckOwn())
  {
    if(!$g9projectdb->ApplyYet())
    {
      echo '<button onclick="location.href=\'project_apply.php?workid='.$_GET['workid'].'\'" type="button" class="btn btn-primary" form="applyjob" value="submit">Apply for this job</button>';
    }
    else
    {
      echo '<button type="button" class="btn btn-primary" disabled="disabled">Already Applied</button>';
    }
  }
  else
  {
  	$g9projectdb->CheckApply();
  }
}
?>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>