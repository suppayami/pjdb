<?php
require_once("./include/g9projectdb_config.php");

if(!$g9projectdb->CheckLogin())
{
    $g9projectdb->RedirectToURL("notlogin.php");
    exit;
}
if(isset($_POST['submitted']))
{
   if($g9projectdb->PostHire())
   {
   	$g9projectdb->RedirectToURL("post_project.php");
   }
}
include 'header.php';
?>

<div id="project-detail">
	<h2 class="sub-header">New Project</h2>

	<div><span class='error'><?php echo $g9projectdb->GetErrorMessage(); ?></span></div>
	<div class="container">
		<form role="form" id='changeinfo' action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
			<input type='hidden' name='submitted' id='submitted' value='1'/>
			<div class="col-sm-3">
				<label for="title">Project Name</label>
    			<input type="text" class="form-control" id="title" name="title" maxlength="128">
				<label for="location">Location</label>
				<select class="form-control" name="location" id="location">
					<?php $g9projectdb->GetOptionsx('location'); ?>
				</select>
				<label for="category">Category</label>
				<select class="form-control" name="category" id="category">
					<?php $g9projectdb->GetOptionsx('category'); ?>
				</select>
			</div>

			<div class="col-sm-9 job-description">
				<label for="description">Description</label>
				<textarea class="form-control" rows="24" id="description" name="description" maxlength="1024"></textarea><br/>
				<button type="submit" class="btn btn-primary" value="submit">Submit Post</button>
			</div>
		</form>
	</div>
</div>

<?php include 'footer.php'; ?>