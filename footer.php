    	</div>
	      <footer>
	        <div class="row">
	          <div class="col-lg-12">

	            <ul class="list-unstyled">
	              <li class="pull-right"><a href="#">Back to top</a></li>
	            </ul>
	            <p>Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>. Icons from <a href="http://fortawesome.github.io/Font-Awesome/" rel="nofollow">Font Awesome</a>. Web fonts from <a href="http://www.google.com/webfonts" rel="nofollow">Google</a>.</p>
	            <p>Bootstrap theme made by <a href="http://thomaspark.me" rel="nofollow">Thomas Park</a>.</p>
	            <ul class="list-unstyled">
		            <li>[DPI3SN] Nguyen Tuan Cuong</li>
		            <li>[DPI3SN] Hoang Long</li>
		            <li>[DPI3SN] Pham Hong An</li>
	            </ul>

	          </div>
	        </div>

	      </footer>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="js/bootstrap.min.js"></script>
	    <script>
			$("#filter-toggle").click( function() {
				$("#filter-toggle-icon").toggleClass("glyphicon-arrow-down");
				$("#filter-toggle-icon").toggleClass("glyphicon-arrow-right");
			});

			$("#who-applied").click( function() {
				$("#who-applied-icon").toggleClass("glyphicon-arrow-down");
				$("#who-applied-icon").toggleClass("glyphicon-arrow-right");
			});
	    </script>
    </div>
  </body>
</html>