<?php
require_once("class.phpmailer.php");
require_once("formvalidator.php");
require_once("NotORM.php");

class G9ProjectDB
{
    var $admin_email;
    var $from_address;
    
    var $username;
    var $pwd;
    var $database;
    var $connection;
    var $rand_key;
    
    var $error_message;
    
    //-----Initialization -------
    function G9ProjectDB()
    {
        $this->sitename = 'http://oamk.yamiworld.com/~pjdb/';
        $this->rand_key = '0iQx5oBk66oVZep';
    }
    
    function InitDB($host,$uname,$pwd,$database)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
    }
    function SetAdminEmail($email)
    {
        $this->admin_email = $email;
    }
    
    function SetWebsiteName($sitename)
    {
        $this->sitename = $sitename;
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function RegisterUser()
    {
        if(!isset($_POST['submitted']))
        {
           return false;
        }
        
        $formvars = array();
        
        if(!$this->ValidateRegistrationSubmission())
        {
            return false;
        }
        
        $this->CollectRegistrationSubmission($formvars);
        
        if(!$this->SaveToDatabase($formvars))
        {
            return false;
        }
        
        if(!$this->SendUserConfirmationEmail($formvars))
        {
            return false;
        }

        $this->SendAdminIntimationEmail($formvars);
        
        return true;
    }

    function ConfirmUser()
    {
        if(empty($_GET['code'])||strlen($_GET['code'])<=10)
        {
            $this->HandleError("Please provide the confirm code");
            return false;
        }
        $user_rec = array();
        if(!$this->UpdateDBRecForConfirmation($user_rec))
        {
            return false;
        }
        
        $this->SendUserWelcomeEmail($user_rec);
        
        $this->SendAdminIntimationOnRegComplete($user_rec);
        
        return true;
    }    
    
    function Login()
    {
        if(empty($_POST['username']))
        {
            $this->HandleError("UserName is empty!");
            return false;
        }
        
        if(empty($_POST['password']))
        {
            $this->HandleError("Password is empty!");
            return false;
        }
        
        $username = trim($_POST['username']);
        $password = trim($_POST['password']);
        
        if(!isset($_SESSION)){ session_start(); }
        if(!$this->CheckLoginInDB($username,$password))
        {
            return false;
        }
        
        $_SESSION[$this->GetLoginSessionVar()] = $username;
        
        return true;
    }
    
    function CheckLogin()
    {
        if(!isset($_SESSION)){ session_start(); }

        $sessionvar = $this->GetLoginSessionVar();

        if(empty($_SESSION[$sessionvar]))
        {
           return false;
        }
        return true;
    }

    function CheckConfirm()
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        $result = $this->ormconnection->user()->select("username")->where("confirmcode","y")->where("email",$this->UserEmail());
        
        if(!$result || count($result) <= 0)
        {
            return false;
        }
        return true;
    }

    function CheckInfo()
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        $result = $this->ormconnection->user()->select("userid","username","gender","location_locationid","phonenumber","description")->where("email",$this->UserEmail());
        
        if(!$result || count($result) <= 0)
        {
            return false;
        }

        $row = $result->fetch(PDO::FETCH_ASSOC);
        $_SESSION['userid'] = $row['userid'];
        $_SESSION['username'] = $row['username'];
        $_SESSION['gender'] = $row['gender'];
        $_SESSION['location'] = $row['location_locationid'];
        $_SESSION['phonenumber']  = $row['phonenumber'];
        $_SESSION['description'] = $row['description'];

        $this->list['skill'] = array();
        $result = $this->ormconnection->skill()->select("category_categoryid")->where("user_userid",$row['userid']);
        foreach($result as $row)
        {
            $this->list['skill'][] = $row["category_categoryid"];
        }
        if(!$this->list['skill'] || count($this->list['skill']) <= 0)
        {
            $this->list['skill'] = array(-3,-2);
        }
        
        return true;
    }

    function GetUserName()
    {
        return isset($_SESSION['username'])?$_SESSION['username']:'';
    }

    function UserFullName()
    {
        return isset($_SESSION['name_of_user'])?$_SESSION['name_of_user']:'';
    }
    
    function UserEmail()
    {
        return isset($_SESSION['email_of_user'])?$_SESSION['email_of_user']:'';
    }
    
    function LogOut()
    {
        session_start();
        
        $sessionvar = $this->GetLoginSessionVar();
        
        $_SESSION[$sessionvar]=NULL;
        
        unset($_SESSION[$sessionvar]);
    }
    
    function EmailResetPasswordLink()
    {
        if(empty($_POST['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        $user_rec = array();
        if(false === $this->GetUserFromEmail($_POST['email'], $user_rec))
        {
            return false;
        }
        if(false === $this->SendResetPasswordLink($user_rec))
        {
            return false;
        }
        return true;
    }
    
    function ResetPassword()
    {
        if(empty($_GET['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        if(empty($_GET['code']))
        {
            $this->HandleError("reset code is empty!");
            return false;
        }
        $email = trim($_GET['email']);
        $code = trim($_GET['code']);
        
        if($this->GetResetPasswordCode($email) != $code)
        {
            $this->HandleError("Bad reset code!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($email,$user_rec))
        {
            return false;
        }
        
        $new_password = $this->ResetUserPasswordInDB($user_rec);
        if(false === $new_password || empty($new_password))
        {
            $this->HandleError("Error updating new password");
            return false;
        }
        
        if(false == $this->SendNewPassword($user_rec,$new_password))
        {
            $this->HandleError("Error sending new password");
            return false;
        }
        return true;
    }
    
    function ChangePassword()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        if(empty($_POST['oldpwd']))
        {
            $this->HandleError("Old password is empty!");
            return false;
        }
        if(empty($_POST['newpwd']))
        {
            $this->HandleError("New password is empty!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }
        
        $pwd = trim($_POST['oldpwd']);
        
        if($user_rec['password'] != md5($pwd))
        {
            $this->HandleError("The old password does not match!");
            return false;
        }
        $newpwd = trim($_POST['newpwd']);
        
        if(!$this->ChangePasswordInDB($user_rec, $newpwd))
        {
            return false;
        }
        return true;
    }
    
    function ChangeInfo()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        if(empty($_POST['oldpwd']))
        {
            $this->HandleError("Current password is empty!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }
        
        $pwd = trim($_POST['oldpwd']);
        
        if($user_rec['password'] != md5($pwd))
        {
            $this->HandleError("The current password does not match!");
            return false;
        }

        $gender = trim($_POST['gender']);
        $skill = (isset($_POST['skill']) ? array_map('trim', $_POST['skill']) : 0);
        $location = trim($_POST['location']);
        $phonenumber = trim($_POST['phonenumber']);
        $description = trim($_POST['description']);

        
        if(!$this->ChangeInfoInDB($user_rec, $gender, $skill, $location, $phonenumber, $description))
        {
            return false;
        }
        return true;
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return "/~pjdb/".basename($_SERVER['SCRIPT_NAME']);
    }    
    
    function SafeDisplay($value_name)
    {
        if(empty($_POST[$value_name]))
        {
            return'';
        }
        return htmlentities($_POST[$value_name]);
    }

    function SafeDisplayDB($value_name)
    {
        if(empty($_POST[$value_name]))
        {
            return htmlentities($_SESSION[$value_name]);
        }
        return htmlentities($_POST[$value_name]);
    }

    function SafeDisplayDBMulti($value_name1)
    {
        if(!empty($_POST[$value_name1]));
        {
            $result = array_map('htmlentities', $this->list[$value_name1]);
            return $result;
        }
        $result = array_map('htmlentities', $_POST[$value_name1]);
        return $result;
    }

    function RedirectToURL($url)
    {
        header("Location: $url");
        exit;
    }
    
    function GetSpamTrapInputName()
    {
        return 'sp'.md5('KHGdnbvsgst'.$this->rand_key);
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }    
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_message .= $err."\r\n";
    }
    
    function GetFromAddress()
    {
        if(!empty($this->from_address))
        {
            return $this->from_address;
        }

        $host = $_SERVER['SERVER_NAME'];

        $from ="nobody@$host";
        return $from;
    }

    function GetLoginSessionVar()
    {
        $retvar = md5($this->rand_key);
        $retvar = 'usr_'.substr($retvar,0,10);
        return $retvar;
    }
    
    function CheckLoginInDB($username,$password)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        $pwdmd5 = md5($password);
        $result = $this->ormconnection->user()->select("name","email")->where("username",$username)->where("password",$pwdmd5);
        
        if(!$result || count($result) <= 0)
        {
            $this->HandleError("Error logging in. The username or password does not match");
            return false;
        }
        
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        
        $_SESSION['name_of_user']  = $row['name'];
        $_SESSION['email_of_user'] = $row['email'];
        
        return true;
    }
    
    function UpdateDBRecForConfirmation(&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        $confirmcode = $_GET['code'];
        
        $result = $this->ormconnection->user()->select("name","email")->where("confirmcode",$confirmcode);

        if(!$result || count($result) <= 0)
        {
            $this->HandleError("Wrong confirm code.");
            return false;
        }

        $row = $result->fetch(PDO::FETCH_ASSOC);
        $user_rec['name'] = $row['name'];
        $user_rec['email']= $row['email'];

        try
        {
            $result = $this->ormconnection->user("confirmcode",$confirmcode)->update(array("confirmcode" => "y"));
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error inserting data to the table!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }
    
    function ResetUserPasswordInDB($user_rec)
    {
        $new_password = substr(md5(uniqid()),0,10);
        
        if(false == $this->ChangePasswordInDB($user_rec,$new_password))
        {
            return false;
        }
        return $new_password;
    }
    
    function ChangePasswordInDB($user_rec, $newpwd)
    {
        try
        {
            $result = $this->ormconnection->user("userid",$user_rec['userid'])->update(array("password" => md5($newpwd)));
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error updating the password!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }
    
    function ChangeInfoInDB($user_rec, $gender, $skill, $location, $phonenumber, $description)
    {
        try
        {
            $result = $this->ormconnection->user("userid",$user_rec['userid'])->update(array("gender" => $gender, "location_locationid" => $location, "phonenumber" => $phonenumber, "description" => $description));
            $result = $this->ormconnection->skill("user_userid",$user_rec['userid'])->delete();
            if($skill)
            {
                foreach ($skill as $row) $result = $this->ormconnection->skill()->insert(array("user_userid" => $user_rec['userid'], "category_categoryid" => $row));
            }
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error updating the info!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }
    
    function GetUserFromEmail($email,&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        $result = $this->ormconnection->user()->where("email",$email);

        if(!$result || count($result) <= 0)
        {
            $this->HandleError("There is no user with email: $email");
            return false;
        }
        $user_rec = $result->fetch(PDO::FETCH_ASSOC);

        return true;
    }
    
    function SendUserWelcomeEmail(&$user_rec)
    {
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($user_rec['email'],$user_rec['name']);
        
        $mailer->Subject = "Welcome to ".$this->sitename;

        $mailer->From = $this->GetFromAddress();        
        
        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "Welcome! Your registration  with ".$this->sitename." is completed.\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;

        if(!$mailer->Send())
        {
            $this->HandleError("Failed sending user welcome email.");
            return false;
        }
        return true;
    }
    
    function SendAdminIntimationOnRegComplete(&$user_rec)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "Registration Completed: ".$user_rec['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$user_rec['name']."\r\n".
        "Email address: ".$user_rec['email']."\r\n";
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function GetResetPasswordCode($email)
    {
       return substr(md5($email.$this->sitename.$this->rand_key),0,10);
    }
    
    function SendResetPasswordLink($user_rec)
    {
        $email = $user_rec['email'];
        
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($email,$user_rec['name']);
        
        $mailer->Subject = "Your reset password request at ".$this->sitename;

        $mailer->From = $this->GetFromAddress();
        
        $link = $this->GetAbsoluteURLFolder().
                '/resetpwded.php?email='.
                urlencode($email).'&code='.
                urlencode($this->GetResetPasswordCode($email));

        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "There was a request to reset your password at ".$this->sitename."\r\n".
        "Please click the link below to complete the request: \r\n".$link."\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function SendNewPassword($user_rec, $new_password)
    {
        $email = $user_rec['email'];
        
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($email,$user_rec['name']);
        
        $mailer->Subject = "Your new password for ".$this->sitename;

        $mailer->From = $this->GetFromAddress();
        
        $mailer->Body ="Hello ".$user_rec['name']."\r\n\r\n".
        "Your password is reset successfully. ".
        "Here is your updated login:\r\n".
        "username:".$user_rec['username']."\r\n".
        "password:$new_password\r\n".
        "\r\n".
        "Login here: ".$this->GetAbsoluteURLFolder()."/login.php\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }    
    
    function ValidateRegistrationSubmission()
    {
        //This is a hidden input field. Humans won't fill this field.
        if(!empty($_POST[$this->GetSpamTrapInputName()]) )
        {
            //The proper error is not given intentionally
            $this->HandleError("Automated submission prevention: case 2 failed");
            return false;
        }
        
        $validator = new FormValidator();
        $validator->addValidation("name","req","Please fill in Name");
        $validator->addValidation("email","email","The input for Email should be a valid email value");
        $validator->addValidation("email","req","Please fill in Email");
        $validator->addValidation("username","req","Please fill in UserName");
        $validator->addValidation("password","req","Please fill in Password");

        
        if(!$validator->ValidateForm())
        {
            $error='';
            $error_hash = $validator->GetErrors();
            foreach($error_hash as $inpname => $inp_err)
            {
                $error .= $inpname.':'.$inp_err."\n";
            }
            $this->HandleError($error);
            return false;
        }        
        return true;
    }
    
    function CollectRegistrationSubmission(&$formvars)
    {
        $formvars['name'] = $_POST['name'];
        $formvars['email'] = $_POST['email'];
        $formvars['username'] = $_POST['username'];
        $formvars['password'] = $_POST['password'];
    }
    
    function SendUserConfirmationEmail(&$formvars)
    {
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($formvars['email'],$formvars['name']);
        
        $mailer->Subject = "Your registration with ".$this->sitename;

        $mailer->From = $this->GetFromAddress();        
        
        $confirmcode = $formvars['confirmcode'];
        
        $confirm_url = $this->GetAbsoluteURLFolder().'/confirm.php?code='.$confirmcode;
        
        $mailer->Body ="Hello ".$formvars['name']."\r\n\r\n".
        "Thanks for your registration with ".$this->sitename."\r\n".
        "Please click the link below to confirm your registration.\r\n".
        "$confirm_url\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;

        if(!$mailer->Send())
        {
            $this->HandleError("Failed sending registration confirmation email.");
            return false;
        }
        return true;
    }

    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';

        $urldir ='';
        $pos = strrpos($_SERVER['REQUEST_URI'],'/');
        if(false !==$pos)
        {
            $urldir = substr($_SERVER['REQUEST_URI'],0,$pos);
        }

        $scriptFolder .= $_SERVER['HTTP_HOST'].$urldir;

        return $scriptFolder;
    }
    
    function SendAdminIntimationEmail(&$formvars)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "New registration: ".$formvars['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$formvars['name']."\r\n".
        "Email address: ".$formvars['email']."\r\n".
        "UserName: ".$formvars['username'];
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function SaveToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }

        if(!$this->IsFieldUnique($formvars,'email'))
        {
            $this->HandleError("This email is already registered");
            return false;
        }
        
        if(!$this->IsFieldUnique($formvars,'username'))
        {
            $this->HandleError("This UserName is already used. Please try another username");
            return false;
        }        
        if(!$this->InsertIntoDB($formvars))
        {
            $this->HandleError("Inserting to Database failed!");
            return false;
        }
        return true;
    }
    
    function IsFieldUnique($formvars,$fieldname)
    {
        $field_val = $formvars[$fieldname];
        $result = $this->ormconnection->user()->select("username")->where($fieldname,$field_val);

        if($result && count($result) > 0)
        {
            return false;
        }
        return true;
    }
    
    function DBLogin()
    {
        try
        {
            $this->connection = new PDO("mysql:host=$this->db_host;dbname=$this->database;charset=utf8",$this->username,$this->pwd);
        }
        catch (PDOException $e)
        {
            $this->HandleError("Database connection failed!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->ormconnection = new NotORM($this->connection);
        return true;
    }

    function InsertIntoDB(&$formvars)
    {
    
        $confirmcode = $this->MakeConfirmationMd5($formvars['email']);
        
        $formvars['confirmcode'] = $confirmcode;

        try
        {
            $result = $this->ormconnection->user()->insert(array("name" => $formvars['name'], "email" => $formvars['email'], "username" => $formvars['username'], "password" => md5($formvars['password']), "confirmcode" => $confirmcode, "gender" => -1, "location_locationid" => -1));
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error inserting data to the table!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }

    function MakeConfirmationMd5($email)
    {
        $randno1 = rand();
        $randno2 = rand();
        return md5($email.$this->rand_key.$randno1.''.$randno2);
    }

    function GetOptions($value_name)
    {

        foreach ($this->ormconnection->$value_name() as $row)
        {
            echo "        <option ".($this->SafeDisplayDB($value_name)==$row[$value_name.'id'] ? "selected " : "")."value='".$row[$value_name.'id']."'>".$row['name']."</option>\r\n";
        }
    }

    function GetOptionsx($value_name)
    {

        foreach ($this->ormconnection->$value_name() as $row)
        {
            echo "        <option value='".$row[$value_name.'id']."'>".$row['name']."</option>\r\n";
        }
    }

    function GetOptionsMulti($value_name,$value_name1)
    {

        foreach ($this->ormconnection->$value_name() as $row)
        {
            //echo "        <option ".(in_array($row[$value_name.'id'], $this->SafeDisplayDBMulti($value_name1)) ? "selected " : "")."value='".$row[$value_name.'id']."'>".$row['name']."</option>\r\n";
            echo '        <div class="checkbox"><label><input type="checkbox" '.(in_array($row[$value_name.'id'], $this->SafeDisplayDBMulti($value_name1)) ? "checked " : "").'name="skill[]" id="skill" value="'.$row[$value_name.'id'].'">'.$row['name'].'</label></div>';
        }
    }

    function GetOptionsMultix($value_name,$value_name1)
    {

        foreach ($this->ormconnection->$value_name() as $row)
        {
            echo "        <option value='".$row[$value_name.'id']."'>".$row['name']."</option>\r\n";
        }
    }

    function GetSearchHire()
    {
        $qry = $this->connection->prepare("SELECT userid,username,email,gender,phonenumber,description
            ,user.name AS user_name
            ,location.name AS location_name
            ,GROUP_CONCAT(category.name SEPARATOR '-') AS category_name
            FROM user
            INNER JOIN location ON location.locationid = user.location_locationid
            INNER JOIN skill ON user.userid = skill.user_userid
            INNER JOIN category ON skill.category_categoryid = category.categoryid
            WHERE location.locationid LIKE ? AND category.categoryid LIKE ? AND user.userid LIKE ?
            GROUP BY user.userid");
        if (!isset($_POST['location']) || trim($_POST['location']) == -1)
        {
            $qry->bindValue(1,'%');
        }
        else
        {
            $qry->bindValue(1,trim($_POST['location']));
        }
        if (!isset($_POST['skill']) || trim($_POST['skill']) == -1)
        {
            $qry->bindValue(2,'%');
        }
        else
        {
            $qry->bindValue(2,trim($_POST['skill']));
        }
        if (!isset($_GET['userid']))
        {
            $qry->bindValue(3,'%');
        }
        else
        {
            $qry->bindValue(3,trim($_GET['userid']));
        }
        return $qry;
    }

    function SearchHire()
    {
        $qry = $this->GetSearchHire();
        $qry->execute();
        $result = $qry->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result))
        {
            echo '<ul>';
            foreach($result as $row)
            {
                echo '<li><a href=showuser.php?userid='.$row['userid'].'>'.$row['user_name'].'</a> | '.$row['gender'].' | '.$row['location_name'].'</li>';
            }
            echo '</ul><br/>';
        }
        else
        {
            echo 'No result!';
        }
    }

    function GetGender($id) {
        $fuck = "";

        if ($id == -1) 
            $fuck .= "Not Specified";
        else if ($id == 0)
            $fuck .= "Female";
        else 
            $fuck .= "Male";

        return $fuck;
    }

    function ShowUser()
    {
        $qry = $this->GetSearchHire();
        $qry->execute();
        $result = $qry->fetch(PDO::FETCH_ASSOC);
        $home_link = "/ProjectDB.edited";
        $skill = explode('-', $result['category_name']);
        //echo '<a href=showuser.php?userid='.$result['userid'].'>'.$result['user_name'].'</a> | '.$result['email'].' | '.$result['gender'].' | '.$result['phonenumber'].' | '.$result['location_name'].' | '.$result['description'].'<br/>';
        //foreach ($skill as $row)
        //    echo $row.'<br/>';
        echo '<h3>'.$result['username'].'\'s Profile</h3><div class="container"><div class="col-sm-3"><dl><dt>Real Name</dt><dd>'.$result['user_name'].'</dd>
            <dt>Gender</dt><dd>'.$this->GetGender($result['gender']).'</dd><dt>Location</dt><dd>'.$result['location_name'].'</dd>
            <dt>Phone number</dt><dd>'.$result['phonenumber'].'</dd>
            <dt>Contact Email</dt><dd><a href="mailto:#">'.$result['email'].'</a></dd><dt>Skills</dt><dd>';
        foreach ($skill as $row)
            echo $row.'<br/>';
        echo '</dd></dl></div><div class="col-sm-9 job-description"><strong>Summary</strong><p>'.nl2br($result['description']).'</p></div></div>';
    }

    function GetSearchWork()
    {
        $qry = $this->connection->prepare("SELECT workid,title,user.userid
            ,work.description AS work_description
            ,user.name AS user_name
            ,location.name AS location_name
            ,category.name AS category_name
            FROM work
            INNER JOIN location ON location.locationid = work.location_locationid
            INNER JOIN user ON user.userid = work.user_userid
            INNER JOIN category ON work.category_categoryid = category.categoryid
            WHERE location.locationid LIKE ? AND category.categoryid LIKE ? AND work.workid LIKE ?
            ORDER BY workid DESC");
        if (!isset($_GET['location']) || trim($_GET['location']) == -1)
        {
            $qry->bindValue(1,'%');
        }
        else
        {
            $qry->bindValue(1,trim($_GET['location']));
        }
        if (!isset($_GET['skill']) || trim($_GET['skill']) == -1)
        {
            $qry->bindValue(2,'%');
        }
        else
        {
            $qry->bindValue(2,trim($_GET['skill']));
        }
        if (!isset($_GET['workid']))
        {
            $qry->bindValue(3,'%');
        }
        else
        {
            $qry->bindValue(3,trim($_GET['workid']));
        }
        return $qry;
    }

    function SearchWork()
    {
        $qry = $this->GetSearchWork();
        $qry->execute();
        $result = $qry->fetchAll(PDO::FETCH_ASSOC);
        $index = 0;

        if (!empty($result))
        {
            //echo '<ul>';
            foreach($result as $row)
            {
                $index = $index + 1;
                //echo '<li><a href=showwork.php?workid='.$row['workid'].'>'.$row['title'].'</a> | '.$row['user_name'].' | '.$row['category_name'].' | '.$row['location_name'].'</li>';
                echo '<tr><td>'.$index.'</td><td><a href=project.php?workid='.$row['workid'].'>'.$row['title'].'</a></td><td>'.$row['user_name'].'</td><td>'.$row['category_name'].'</td><td>'.$row['location_name'].'</td></tr>';
            }
            //echo '</ul><br/>';
        }
        else
        {
            //echo 'No result!';
        }
    }

    function ShowWork()
    {
        $qry = $this->GetSearchWork();
        $qry->execute();
        $result = $qry->fetch(PDO::FETCH_ASSOC);
        $home_link = "/ProjectDB.edited";
        //echo '<a href=showwork.php?workid='.$result['workid'].'>'.$result['title'].'</a> | '.$result['user_name'].' | '.$result['category_name'].' | '.$result['location_name'].'<br/>'.$result['work_description'];
        echo '<h3>'.$result['title'].'</h3><div class="container"><div class="col-sm-3">
            <dl><dt>Creator</dt><dd><a href=profile.php?userid='.$result['userid'].'>'.$result['user_name'].'</a></dd>
            <dt>Location</dt><dd>'.$result['location_name'].'</dd><dt>Category</dt><dd>'.$result['category_name'].'</dd></dl>
            '.($this->CheckOwn() ? '<button type="button" class="btn btn-primary" onclick="location.href=\''.$home_link.'/project_edit.php?workid='.$result['workid'].'\'">Edit Project</button>' : "").'
            </div><div class="col-sm-9 job-description"><strong>Description</strong><p>'.nl2br($result['work_description']).'</p>';
    }

    function PostHire()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }

        $title = trim($_POST['title']);
        $location = trim($_POST['location']);
        $category = trim($_POST['category']);
        $description = trim($_POST['description']);
        
        if(!$this->PostHireInDB($user_rec, $title, $location, $category, $description))
        {
            return false;
        }
        return true;
    }

    function PostHireInDB($user_rec, $title, $location, $category, $description)
    {
        try
        {
            $result = $this->ormconnection->work()->insert(array("title" => $title, "user_userid" => $user_rec['userid'], "location_locationid" => $location, "category_categoryid" => $category, "description" => $description));
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error updating the info!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }

    function CheckApply()
    {
        $qry = $this->connection->prepare("SELECT userid,email,gender,phonenumber,description
            ,user.name AS user_name
            ,location.name AS location_name
            ,category.name AS category_name
            FROM user
            INNER JOIN apply ON apply.user_userid = user.userid
            INNER JOIN location ON location.locationid = user.location_locationid
            INNER JOIN skill ON user.userid = skill.user_userid
            INNER JOIN category ON skill.category_categoryid = category.categoryid
            WHERE apply.work_workid LIKE ?
            GROUP BY user.userid");
        $qry->bindValue(1,trim($_GET['workid']));
        $qry->execute();
        $result = $qry->fetchAll(PDO::FETCH_ASSOC);
        $index = 0;
        echo '<h3 id="who-applied" class="sub-header text-primary col-button" data-toggle="collapse" data-target="#applied-users">Who applied for this job     <span id="who-applied-icon" class="glyphicon glyphicon-arrow-down"></span></h3>';
        if (!empty($result))
        {
            echo '<div class="collapse in" id="applied-users"><table class="table table-striped"><thead><tr>
                <th>#</th><th>Username</th><th>Location</th><th>Gender</th></tr></thead><tbody>';
            foreach($result as $row)
            {
                $index = $index + 1;
                //echo '<li><a href=showuser.php?userid='.$row['userid'].'>'.$row['user_name'].'</a> | '.$row['gender'].' | '.$row['location_name'].'</li>';
                echo '<tr><td>'.$index.'</td><td><a href=profile.php?userid='.$row['userid'].'>'.$row['user_name'].'</a></td><td>'.$row['location_name'].'</td><td>'.$row['gender'].'</td>';
            }
            echo ' </tbody></table></div>';
        }
        else
        {
            echo 'No one apply yet!';
        }
    }

    function CheckOwn()
    {
        $result = $this->ormconnection->work()->select("user_userid")->where("workid",$_GET['workid']);
        
        if(!$result || count($result) <= 0)
        {
            return false;
        }

        $row = $result->fetch(PDO::FETCH_ASSOC);
        if($row['user_userid'] != $_SESSION['userid'])
        {
            return false;
        }
        return true;
    }

    function GetWorkTitle()
    {
        $qry = $this->GetSearchWork();
        $qry->execute();
        $result = $qry->fetch(PDO::FETCH_ASSOC);
        $home_link = "/ProjectDB.edited";
        return htmlentities($result['title']);
    }

    function GetWorkDescription()
    {
        $qry = $this->GetSearchWork();
        $qry->execute();
        $result = $qry->fetch(PDO::FETCH_ASSOC);
        $home_link = "/ProjectDB.edited";
        return htmlentities($result['work_description']);
    }

    function ChangeHire()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }

        $workid = trim($_POST['workid']);
        $title = trim($_POST['title']);
        $location = trim($_POST['location']);
        $category = trim($_POST['category']);
        $description = trim($_POST['description']);
        
        if(!$this->ChangeHireInDB($user_rec, $workid, $title, $location, $category, $description))
        {
            return false;
        }
        return true;
    }

    function ChangeHireInDB($user_rec, $workid, $title, $location, $category, $description)
    {
        try
        {
            $result = $this->ormconnection->work("workid",$workid)->update(array("title" => $title, "location_locationid" => $location, "category_categoryid" => $category, "description" => $description));
        }
        catch (PDOException $e)
        {
            $this->HandleError("Error updating the info!\r\nMySQL Error: ".$e->getMessage());
            return false;
        }
        return true;
    }

    function DelHire()
    {
        $workid = trim($_GET['workid']);
        $result = $this->ormconnection->work("workid",$workid)->delete();
    }

    function ApplyYet()
    {
        $result = $this->ormconnection->apply()->where("work_workid",$_GET['workid'])->where("user_userid",$_SESSION['userid']);
        
        if(!$result || count($result) <= 0)
        {
            return false;
        }
        return true;
    }
    function ApplyHire()
    {
        $workid = trim($_GET['workid']);
        $result = $this->ormconnection->apply()->insert(array("user_userid" => $_SESSION['userid'], "work_workid" => $workid));
        return true;
    }
}
?>