<?php
  require_once("./include/g9projectdb_config.php");
  $g9projectdb->CheckLogin();
  $g9projectdb->CheckInfo();
  // Change this to home link
  $home_link = "/~pjdb";

  if(isset($_POST['submittedlogin']))
  {
    if($g9projectdb->Login())
    {
      $g9projectdb->RedirectToURL("login.php");
    }
  }
  if(isset($_POST['submittedchgpwd']))
  {
    if($g9projectdb->ChangePassword())
    {
      $g9projectdb->RedirectToURL("update_pwd.php");
    }
  }
  if(isset($_POST['submitted']))
  {
    if($g9projectdb->RegisterUser())
    {
      $g9projectdb->RedirectToURL("signup.php");
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Finding</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Main CSS -->
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#fixed-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="<?php echo $home_link; ?>">Job Finding</a>

        </div><!-- /.navbar-header -->

        <div class="collapse navbar-collapse" id="fixed-navbar">

          <ul class="nav navbar-nav">
            <li><a href="<?php echo $home_link; ?>">Home</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#top-header">Top</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
          <?php if($g9projectdb->CheckLogin()) {
          echo '<li><a href="'.$home_link.'/profile.php?userid='.$_SESSION['userid'].'">Welcome Back '.$g9projectdb->UserFullName().'!</a></li>' ?>
            <li><a href="<?php echo $home_link."/profile_edit.php"; ?>">Edit Profile</a></li>
            <li><a href="#" data-toggle="modal" data-target="#passwordForm">Change Password</a></li>
            <li><a href="<?php echo $home_link."/logout.php"; ?>">Log Out</a></li>
          <?php } else { ?>
            <li><a href="#" data-toggle="modal" data-target="#signupForm">Sign Up</a></li>
            <li><a href="#" data-toggle="modal" data-target="#loginForm">Sign In</a></li>
          <?php } ?>
          </ul>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
      <div class="main-body">
        <!-- login -->
        <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="loginTitle">Sign In</h4>
              </div>
              <div class="modal-body">
                <form role="form" class="form-horizontal" id='login' action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

                  <input type='hidden' name='submittedlogin' id='submitted' value='1'/>
                  <div class="form-group">
                    <label for="loginUsername" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="loginUsername" placeholder="Username..." autocomplete="off" name='username' value='<?php echo $g9projectdb->SafeDisplay('username') ?>' maxlength="32">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="loginPassworld" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="loginPassworld" placeholder="Password..." name='password' maxlength="32">
                    </div>
                  </div>

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit" form="login">Sign In</button>
              </div>
            </div>
          </div>
      </div>

      <!-- signup -->
      <div class="modal fade" id="signupForm" tabindex="-1" role="dialog" aria-labelledby="signupTitle" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="signupTitle">Sign Up</h4>
              </div>
              <div class="modal-body">
                <form role="form" class="form-horizontal" id="signup" action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

                  <input type='hidden' name='submitted' id='submitted' value='1'/>
                  <div class="form-group">
                    <label for="loginFullname" class="col-sm-3 control-label">Fullname</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="loginFullname" placeholder="Fullname..." autocomplete="off" name='name' maxlength="64">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="loginEmail" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="loginEmail" placeholder="Email..." autocomplete="off" name='email' maxlength="64">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="loginUsername" class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="loginUsername" placeholder="Username..." autocomplete="off" name='username' maxlength="32">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="loginPassworld" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="loginPassworld" placeholder="Password..." name='password' maxlength="32">
                    </div>
                  </div>

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit" form="signup">Sign Up</button>
              </div>
            </div>
          </div>
      </div>

      <!-- password -->
      <div class="modal fade" id="passwordForm" tabindex="-1" role="dialog" aria-labelledby="passwordTitle" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="passwordTitle">Change Password</h4>
              </div>
              <div class="modal-body">
                <form role="form" class="form-horizontal" id="changepassword" action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

                  <input type='hidden' name='submittedchgpwd' id='submitted' value='1'/>
                  <div class="form-group">
                    <label for="newPassword" class="col-sm-4 control-label">New Password</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="newPassword" placeholder="New password" autocomplete="off" name='newpwd' maxlength="32">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="currentPassword" class="col-sm-4 control-label">Current Password</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="currentPassword" placeholder="Current password..." name='oldpwd' maxlength="32">
                    </div>
                  </div>

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit" form="changepassword">Change Password</button>
              </div>
            </div>
          </div>
      </div>