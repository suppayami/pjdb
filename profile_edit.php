<?php
require_once("./include/g9projectdb_config.php");

if(!$g9projectdb->CheckLogin())
{
    $g9projectdb->RedirectToURL("notlogin.php");
    exit;
}

$g9projectdb->CheckInfo();

if(isset($_POST['submitted']))
{
   if($g9projectdb->ChangeInfo())
   {
        $g9projectdb->RedirectToURL("update_profile.php");
   }
}
include 'header.php';
?>

<div id="project-detail">
	<h2 class="sub-header">Edit Profile</h2>

	<h3><?php echo $g9projectdb->GetUserName(); ?>'s Profile</h3>

	<div><span class='error'><?php echo $g9projectdb->GetErrorMessage(); ?></span></div>
	<div class="container">
		<form role="form" id='changeinfo' action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
			<input type='hidden' name='submitted' id='submitted' value='1'/>
			<div class="col-sm-3">
    			<label for="gender">Gender</label><br/>
				<select class="form-control" name="gender" id="gender">
					<option <?php echo ($g9projectdb->SafeDisplayDB('gender')=='-1' ? "selected " : "")?>value="-1">Not specified</option>
					<option <?php echo ($g9projectdb->SafeDisplayDB('gender')=='0' ? "selected " : "")?>value="0">Female</option>
					<option <?php echo ($g9projectdb->SafeDisplayDB('gender')=='1' ? "selected " : "")?>value="1">Male</option>
				</select>
				<label for="location">Location</label>
				<select class="form-control" name="location" id="location">
					<?php $g9projectdb->GetOptions('location'); ?>
				</select>
    			<label for="phonenumber">Phone Number</label>
    			<input type="text" class="form-control" id="phonenumber" name="phonenumber" value='<?php echo $g9projectdb->SafeDisplayDB('phonenumber') ?>' maxlength="16">
    			<label>Skills</label>
    			<div style="overflow-y: auto; height:112px; margin-top: -12px;">
					<?php $g9projectdb->GetOptionsMulti('category','skill'); ?>
    			</div>
    			<label for="oldpwd">Current Password</label>
    			<input type="password" class="form-control" id="oldpwd" name="oldpwd" maxlength="32">
			</div>

			<div class="col-sm-9 job-description">
				<label for="description">Summary</label>
				<textarea class="form-control" rows="24" id="description" name="description" maxlength="1024"><?php echo $g9projectdb->SafeDisplayDB('description') ?></textarea><br/>
				<button type="submit" class="btn btn-primary" value="submit">Change Profile</button>
			</div>
		</form>
	</div>
</div>

<?php include 'footer.php'; ?>