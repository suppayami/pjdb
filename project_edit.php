<?php
require_once("./include/g9projectdb_config.php");

if(!$g9projectdb->CheckLogin())
{
    $g9projectdb->RedirectToURL("notlogin.php");
    exit;
}
if(isset($_POST['submitted']))
{
   if($g9projectdb->ChangeHire())
   {
   	$g9projectdb->RedirectToURL("update_project.php");
   }
}
if(!isset($_GET['workid']))
    {
    	$g9projectdb->RedirectToURL("no_permission.php");
    }
include 'header.php';
if(isset($_GET['workid']))
{
    if($g9projectdb->CheckOwn())
    {
?>

<div id="project-detail">
	<h2 class="sub-header">Edit Project</h2>

	<h3><?php echo $g9projectdb->GetWorkTitle(); ?></h3>

	<div><span class='error'><?php echo $g9projectdb->GetErrorMessage(); ?></span></div>
	<div class="container">
		<form role="form" id='changehire' action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
			<input type='hidden' name='submitted' id='submitted' value='1'/>
			<input type='hidden' name='workid' id='submitted' value='<?php echo $_GET['workid']; ?>'/>
			<div class="col-sm-3">
				<label for="title">Project Name</label>
    			<input type="text" class="form-control" id="title" value="<?php echo $g9projectdb->GetWorkTitle(); ?>" name="title" maxlength="128">
				<label for="location">Location</label>
				<select class="form-control" name="location" id="location">
					<?php $g9projectdb->GetOptionsx('location'); ?>
				</select>
				<label for="category">Category</label>
				<select class="form-control" name="category" id="category">
					<?php $g9projectdb->GetOptionsx('category'); ?>
				</select>
			</div>

			<div class="col-sm-9 job-description">
				<label for="description">Description</label>
				<textarea class="form-control" rows="24" id="description" name="description" maxlength="1024"><?php echo nl2br($g9projectdb->GetWorkDescription()); ?></textarea><br/>
				<button type="submit" class="btn btn-primary" value="submit">Submit Post</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#delete-confirm">Delete Post</button>
			</div>
		</form>
	</div>
</div>

      <div class="modal fade" id="delete-confirm" tabindex="-1" role="dialog" aria-labelledby="deleteTitle" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="passwordTitle">Confirm Delete</h4>
              </div>
              <div class="modal-body">
                <form role="form" class="form-horizontal" id="deleteForm">
			        

                Do you want to delete this post?

                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button <?php echo 'onclick="location.href=\'project_del.php?workid='.$_GET['workid'].'\'"';?> type="button" class="btn btn-primary" value="submit" form="deleteForm">Delete</button>
              </div>
            </div>
          </div>
      </div>

<?php } }
include 'footer.php'; ?>