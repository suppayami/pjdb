<?php 
require_once("./include/g9projectdb_config.php");
if(isset($_GET['code']))
{
   if($g9projectdb->ConfirmUser())
   {
        $g9projectdb->RedirectToURL("confirmed.php");
   }
}
include 'header.php';?>
<div><span class='error'><?php echo $g9projectdb->GetErrorMessage(); ?></span></div>
<div id="project-detail">
	<h2 class="sub-header">Confirm registration</h2>

	<div class="container">
		<form role="form" id='confirm' action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='get' accept-charset='UTF-8'>
			<div class="col-sm-3">
				<label for="title">Confirmation Code:</label>
    			<input type="text" class="form-control" id="code" name="code" maxlength="32"><br/>
    			<button type="submit" class="btn btn-primary" value="submit">Submit Post</button>
			</div>
		</form>
	</div>

<?php include 'footer.php'; ?>