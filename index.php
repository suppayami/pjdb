<?php include 'header.php'; ?>

    <div class="jumbotron" id="top-header">
      <div class="container">
        <h1>Job Finding</h1>
        <p>This is a widely job site where businesses connect with independent service providers to outsource their work and employee can find projects and apply for job in Finland.</p>
      </div>
    </div>
    <div><span class='error'><?php echo $g9projectdb->GetErrorMessage(); ?></span></div>

    <div id="filter" class="filter">
    <form action='<?php echo $g9projectdb->GetSelfScript(); ?>' method='get' accept-charset='UTF-8'>
      <input type='hidden' name='submittedsch' id='submitted' value='1'/>
      <h2 id="filter-toggle" class="sub-header col-button" data-toggle="collapse" data-target="#filter-buttons">Filter    <span id="filter-toggle-icon" class="glyphicon glyphicon-arrow-down"></span></h2>
      <div class="filter-buttons collapse in" id="filter-buttons">
        <p class="text-info">Filter projects list by project's category and location.</p>
        <div class="row">

          <div class="col-md-6">
          <div class="input-group">
            <span class="input-group-addon">Category</span>
            <select class="form-control" type='select' name="skill" id='skill'>
              <option value='-1'>All</option>
              <?php $g9projectdb->GetOptionsx('category'); ?>
            </select>
          </div>
          </div>

          <div class="col-md-6">
          <div class="input-group">
            <span class="input-group-addon">Location</span>
            <select class="form-control" type='select' name='location' id='location'>
              <?php $g9projectdb->GetOptionsx('location'); ?>
            </select>
          </div>
          </div>

        </div>
        <button type="submit" class="btn btn-primary" value="submit">Search</button>
      </div>
    </div>

    <div id="projects-list">
      <h2 class="sub-header">Projects List</h2>

      <div class="table-responsive">

        <table class="table table-striped table-hover">

          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Poster</th>
              <th>Category</th>
              <th>Location</th>
            </tr>
          </thead>

          <tbody>
<?php
if(isset($_POST['submittedsch']))
{
    $g9projectdb->SearchWork();
}
else
{
    $g9projectdb->SearchWork();
}
?>
          </tbody>

        </table>
        <!--<ul class="pagination">
          <li><a href="#">&laquo;</a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">&raquo;</a></li>
        </ul>-->
      </div>

    </div>
    </form>

<?php include 'footer.php'; ?>
