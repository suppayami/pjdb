<?php
require_once("./include/g9projectdb_config.php");

if(!$g9projectdb->CheckLogin())
{
    $g9projectdb->RedirectToURL("notlogin.php");
    exit;
}
//include 'header.php';
?>

<?php
  require_once("./include/g9projectdb_config.php");
  $g9projectdb->CheckLogin();
  $g9projectdb->CheckInfo();
  // Change this to home link
  $home_link = "/~pjdb";

  if(isset($_POST['submittedlogin']))
  {
    if($g9projectdb->Login())
    {
      $g9projectdb->RedirectToURL("login.php");
    }
  }
  if(isset($_POST['submittedchgpwd']))
  {
    if($g9projectdb->ChangePassword())
    {
      $g9projectdb->RedirectToURL("update_pwd.php");
    }
  }
  if(isset($_POST['submitted']))
  {
    if($g9projectdb->RegisterUser())
    {
      $g9projectdb->RedirectToURL("signup.php");
    }
  }
?>

<?php
if(isset($_GET['workid']))
{
  if(!$g9projectdb->CheckOwn())
  {
    if(!$g9projectdb->ApplyYet())
    {
      $g9projectdb->ApplyHire();
    }
  }
}
?>
<head>
<title>Applying...</title>
</head>
<script language="javascript">
    window.location.href = "./project.php?workid=<?php echo $_GET['workid']; ?>"
</script>
<?php //include 'footer.php'; ?>